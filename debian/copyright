Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: fontmatrix
Upstream-Contact: Frédéric Coiffier
Source: https://github.com/fcoiffie/fontmatrix
Files-Excluded: src/graphic-resources/about-vectors.svg src/icons/application-fontmatrix-systray-vectors.svg src/icons/application-fontmatrix-vectors.svg

Files: *
Copyright: 2012-2020 Frédéric Coiffier (QT5 port)
                     Alessandro Rimoldi <a.l.e@ideale.ch>
                     Eclypse <eclipse.magick@gmail.com>
                     Peter Linnell <mrdocs@scribus.info>
                     Parag Nemade <pnemade@redhat.com>
                     Alexandre Prokoudine <alexandre.prokoudine@gmail.com>
                     Riku Leino <riku@scribus.info>
                     Vladimir Savic <vladimir.firefly.savic@gmail.com>
           2007-2011 Pierre Marchand <pierre@oep-h.com>
License: GPL-2+

Files: pythonqt/*
Copyright: 2006 MeVis Research GmbH All Rights Reserved.
License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 Further, this software is distributed without any warranty that it is
 free of the rightful claim of any third person regarding infringement
 or the like.  Any license provided herein, whether implied or
 otherwise, applies only to this software file.  Patent licenses, if
 any, provided herein do not apply to combinations of this program with
 other software, or any other product whatsoever.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA.
 .
 Contact information: MeVis Research GmbH, Universitaetsallee 29,
 28359 Bremen, Germany or:
 .
 http://www.mevis.de

Files: harfbuzz/*
Copyright: 1998-2004 David Turner and Werner Lemberg
           2004,2007 Red Hat, Inc. (Owen Taylor, Behdad Esfahbod)
           2008 Nokia Corporation and/or its subsidiary(-ies)
License: MIT

License: MIT
 Permission is hereby granted, without written agreement and without
 license or royalty fees, to use, copy, modify, and distribute this
 software and its documentation for any purpose, provided that the
 above copyright notice and the following two paragraphs appear in
 all copies of this software.
 .
 IN NO EVENT SHALL THE COPYRIGHT HOLDER BE LIABLE TO ANY PARTY FOR
 DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN
 IF THE COPYRIGHT HOLDER HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.
 .
 THE COPYRIGHT HOLDER SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING,
 BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 ON AN "AS IS" BASIS, AND THE COPYRIGHT HOLDER HAS NO OBLIGATION TO
 PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

Files: help/js/*
Copyright: 2007 John Resig
License: MIT

Files: src/graphic-resources/html-css/fontmatrix.js
Copyright: 2010 The Dojo Foundation
           2010 John Resig
License: DOJO-MIT or GPL-2-or-later
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

Files: debian/*
Copyright: 2017-2018 Gürkan Myczko <gurkan@phys.ethz.ch>
           2007-2011 Oleksandr Moskalenko <malex@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
